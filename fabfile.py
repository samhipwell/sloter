from __future__ import with_statement
import os
from fabric.api import cd, local, run


def setup_virtualenv():
    '''
    * setup virtualenv
    '''
    local('python3 -m venv .env --without-pip')
    local('wget https://bootstrap.pypa.io/get-pip.py -P .env/')
    local('.env/bin/python3 .env/get-pip.py')
    local('cp config/activate_this.py .env/bin/activate_this.py')


def pip_install():
    '''
    * install dependcies
    '''
    local('.env/bin/pip3 install -r requirements.txt')


def npm_install():
    '''
    * install JS dependencies
    '''
    local('npm install')


def build():
    '''
    * compiles the frontend scss and js
    '''
    local('npm run production')

def migrate():
    local('.env/bin/python3 manage.py migrate')

def loaddata():
    local('.env/bin/python3 manage.py loaddata config/user.json', capture=False)
    local('.env/bin/python3 manage.py loaddata config/planner.json', capture=False)


def install():
    os.chdir(os.path.dirname(__file__))
    setup_virtualenv()
    pip_install()
    npm_install()
    build()
    migrate()
    loaddata()

def serve():
    local('.env/bin/python3 manage.py runserver')
