import jquery from 'jquery';
window.jQuery = jquery;
window.$ = jquery;

const WOW = require('wowjs'); window.wow = new WOW.WOW({
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {

    },
    scrollContainer: null // optional scroll container selector, otherwise use window
});
window.wow.init();
import Vue from 'vue'
import Datepicker from 'vuejs-datepicker';
import slotschart from './components/slotschart';
import deliverytype from './components/deliverytype';
let moment = require('moment');
let axios = require('axios');
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';


if(document.querySelector('#slotspage')){
    new Vue({
        el:"#slotspage",
        components: {
            Datepicker,
            slotschart,
            deliverytype
        },
        data: function(){
            return {
                date: undefined,
                slots:undefined,
                grouping:0,
                selectSlot:undefined,
                delivery: undefined
            }
        },
        methods: {
            setDate: function(e){
                this.date = e;
                this.getSlots();
            },
            getSlots: function(e){
                let elem = this;
                let request = '/api/date/' + elem.date.getFullYear() +'/' + (parseInt(elem.date.getUTCMonth()) + 1) + '/' + elem.date.getDate() +'/';
                if(elem.delivery != undefined){
                    request = request + 'd/' + elem.delivery;
                }
                axios.get(request).then(function(resp){
                    console.log(resp.data);
                    elem.slots = resp.data;
                }, function(error){
                    console.log(error);
                })
            },
            goPrevious:function(){
                let elem = this;
                let newdate = moment(elem.date);
                elem.setDate(newdate.subtract(elem.grouping, 'days').toDate())
            },
            goNext:function(){
                let elem = this;
                let newdate = moment(elem.date);
                elem.setDate(newdate.add(elem.grouping, 'days').toDate())
            },
            submitSlot: function(event){
                let slectdate = moment(event.date).toDate();
                let request = '/api/date/' + slectdate.getFullYear() +'/' + (parseInt(slectdate.getUTCMonth()) + 1) + '/' + slectdate.getDate() +'/' + event.slot.id;

                axios.post(request, {}).then(function(resp){
                    console.log(resp.data)
                }, function(error){
                    console.log(error);
                });
            },
            deliverySelect: function(event){
                this.delivery = event;
            }
        },
        mounted(){
            this.date = new Date();
            this.grouping = parseInt(document.getElementById('slotspage').getAttribute('data-grouping'));
            this.getSlots();
        }

    })
}