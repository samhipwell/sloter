from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from . import views
from . import apis

# API router to return the API requests to the page
router = DefaultRouter()
router.register(r'timeslit', apis.TimeSlotView)
router.register(r'types', apis.DeliveryTypeView)
router.register(r'days', apis.DelieryDaysView)
router.register(r'planner', apis.PlannerView)

urlpatterns = [
    # More specifc elements that
    url(r'^api/date/(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/$', views.timeslot_return, name='dates'),
    url(r'^api/date/(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<slot>\d)$', views.timeslot_book, name='dates'),
    url(r'^api/date/(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/d/(?P<delivery>[-\w]+)$', views.timeslot_return, name='dates'),

    url(r'^api/', include(router.urls)),
    url(r'^$', views.mainpage, name='index'),
]