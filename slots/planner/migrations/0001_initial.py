# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2017-12-09 00:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DeliveryDays',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day', models.IntegerField(choices=[(0, 'Monday'), (1, 'Tuesday'), (2, 'Wednesday'), (3, 'Thursday'), (4, 'Friday'), (5, 'Saturday'), (6, 'Sunday')], default=0, verbose_name='Day')),
                ('allow_delivery', models.NullBooleanField(default=None, help_text='True for allow, False for Not allow', verbose_name='Allow Delivery')),
                ('nth_in_month', models.IntegerField(blank=True, null=True, verbose_name='Nth day repeated in the Month')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Delivery Day',
                'verbose_name_plural': 'Delivery Days',
                'ordering': ['day', 'timeslot__title'],
            },
        ),
        migrations.CreateModel(
            name='DeliveryTypes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True, verbose_name='Name')),
                ('slug', django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from=('name',))),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Delivery Type',
                'verbose_name_plural': 'Delivery Types',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Planner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Date')),
                ('taken', models.BooleanField(default=False, verbose_name='Taken')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='TimeSlot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Slot')),
                ('start', models.TimeField(verbose_name='Start Time')),
                ('end', models.TimeField(verbose_name='End Time')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Time Slot',
                'verbose_name_plural': 'Time Slot',
                'ordering': ['start', 'end'],
            },
        ),
        migrations.AddField(
            model_name='planner',
            name='slot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='planner.TimeSlot'),
        ),
        migrations.AddField(
            model_name='deliverydays',
            name='deliverytype',
            field=models.ManyToManyField(to='planner.DeliveryTypes', verbose_name='Delivery Types'),
        ),
        migrations.AddField(
            model_name='deliverydays',
            name='timeslot',
            field=models.ManyToManyField(to='planner.TimeSlot', verbose_name='Time Slot'),
        ),
    ]
