from django.db import models
from django.db.models import Q
from django_extensions.db.fields import AutoSlugField, CreationDateTimeField, ModificationDateTimeField
from django.utils.translation import ugettext_lazy as _


class TimeSlot(models.Model):
    title = models.CharField(_('Slot'), max_length=255)
    start = models.TimeField(_('Start Time'))
    end = models.TimeField(_('End Time'))
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __str__(self):
        return "{0} ({1}-{2})".format(self.title, self.start, self.end)

    class Meta:
        verbose_name = 'Time Slot'
        verbose_name_plural = 'Time Slot'
        ordering = ['start', 'end',]


class DeliveryTypes(models.Model):
    name = models.CharField(_('Name'), max_length=255, unique=True)
    slug = AutoSlugField(populate_from=('name',))
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __str__(self):
        return "{0}".format(self.name)

    class Meta:

        verbose_name = 'Delivery Type'
        verbose_name_plural = 'Delivery Types'
        ordering = ['name',]


class DeliveryDaysManager(models.Manager):
    def getTimeSlots(self, weekday, nth=0, deliverytype=None):
        """
        This takes the different query elements and works out what Delivery days are available
        :param weekday: the day of the week 0-6
        :param nth: the repeating number of the week
        :param deliverytype: the type of delivery
        :return: list of available time slots
        """
        # This generates the query elements and then produces a filter options that can be used
        opt = []
        if deliverytype:
            opt.append(Q(deliverytype=deliverytype))
        if nth > 0:
            opt.append((Q(nth_in_month=nth) | Q(nth_in_month__isnull=True)))
        opt.append(Q(day=weekday))

        qry = opt.pop()
        for q_ in opt:
            qry &= q_

        #
        non_slots = self.get_queryset().filter(allow_delivery=False).filter(qry).values_list('timeslot', flat=True)
        allow_slots = self.get_queryset().filter(allow_delivery=True).filter(qry).values_list('timeslot', flat=True)

        return TimeSlot.objects.filter(pk__in=list(set(non_slots) ^ set(allow_slots)))


class DeliveryDays(models.Model):
    DATES = (
        (0, _('Monday')),
        (1, _('Tuesday')),
        (2, _('Wednesday')),
        (3, _('Thursday')),
        (4, _('Friday')),
        (5, _('Saturday')),
        (6, _('Sunday')),
    )
    day = models.IntegerField(_('Day'), choices=DATES, default=0)
    deliverytype = models.ManyToManyField(DeliveryTypes, verbose_name=_('Delivery Types'))
    timeslot = models.ManyToManyField(TimeSlot, verbose_name=_('Time Slot'))
    allow_delivery = models.NullBooleanField(_('Allow Delivery'), help_text=_('True for allow, False for Not allow'), default=None)
    nth_in_month = models.IntegerField(_('Nth day repeated in the Month'), blank=True, null=True)
    objects = DeliveryDaysManager()
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __str__(self):
        return "{0} {1}".format(self.day, ', '.join(self.deliverytype.values_list('name', flat=True)))

    class Meta:
        verbose_name = 'Delivery Day'
        verbose_name_plural = 'Delivery Days'
        ordering = ['day',]


class PlannerManager(models.Manager):
    def check_exists(self, date, slots):
        plannerlist = []
        for slot in slots:
            planner, created = self.get_or_create(date=date, slot=slot)
            plannerlist.append(planner.id)
        return plannerlist


class Planner(models.Model):
    date = models.DateField(_('Date'))
    slot = models.ForeignKey(TimeSlot)
    taken = models.NullBooleanField(_('Taken'), default=False)
    objects = PlannerManager()
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __str__(self):
        return "{0} - {1}".format(self.date, self.slot.title)