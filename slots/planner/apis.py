from rest_framework import viewsets
from .models import *
from .serializers import *


class TimeSlotView(viewsets.ReadOnlyModelViewSet):
    """
    This will be the Viewset for the timeslots
    """
    queryset = TimeSlot.objects.all()
    serializer_class = TimeSlotSerializer


class DeliveryTypeView(viewsets.ReadOnlyModelViewSet):
    """
    The View set for the Delivery Type
    """
    queryset = DeliveryTypes.objects.all()
    serializer_class = DeliveryTypeSerializer


class DelieryDaysView(viewsets.ReadOnlyModelViewSet):
    """
    Delviery days view set
    """
    queryset = DeliveryDays.objects.all()
    serializer_class = DeliveryDaysSerializer


class PlannerView(viewsets.ReadOnlyModelViewSet):
    """
    Planner view set
    """
    queryset = Planner.objects.all()
    serializer_class = PlannerSerializer
