from django.http import JsonResponse, Http404
from django.shortcuts import render
from datetime import datetime, date, timedelta

from rest_framework.response import Response

from slots.planner.serializers import PlannerSerializer
from .models import  *
from django.conf import settings
from calendar import monthrange
from rest_framework import generics, mixins


def daycount(checkdate, weekday=None):
    """
    This works out the nth day of the month from the date and what day
    :param checkdate: date to be checked to get the year and month data
    :param weekday: week day integer 0-6 of what you want to check repetition, if none then takes day of checkdate
    :return: integer of the repetition number 1 - 4
    """
    if not weekday or weekday > 6:
        weekday = checkdate.weekday()
    num_days = monthrange(checkdate.year, checkdate.month)[1]
    days = [date(checkdate.year, checkdate.month, day) for day in range(1, num_days+1)
            if date(checkdate.year, checkdate.month, day).weekday() == weekday]
    try:
        return days.index(checkdate) + 1
    except:
        return 0


def getDaySlots(checkdate=None, deliverytype=None):
    """
    This function generates the possible timeslots for the number of days given
    :param checkdate:
    :return:
    """
    if not checkdate:
        checkdate = datetime.now().date()
    date_listings = []
    current_count = 0
    deadcount = 0
    # Loops through to make sure there is enough days produced for the table
    while current_count < settings.DAY_GROUPING:
        weekday = checkdate.weekday()
        repnum = daycount(checkdate, weekday)
        deliverytimeslot = list(DeliveryDays.objects.getTimeSlots(weekday, nth=repnum, deliverytype=deliverytype))
        if len(deliverytimeslot) != 0:
            date_listings.extend(Planner.objects.check_exists(date=checkdate, slots=deliverytimeslot))
            current_count += 1
        checkdate = checkdate + timedelta(days=1)
        deadcount += 1
        # This is here to make sure the loop doesn't go on indefinently
        if deadcount >= settings.MAX_DAY_SEARCH:
            break
    return date_listings


def mainpage(request):
    return render(request, 'mainpage.html', context={
        'day_grouping': settings.DAY_GROUPING
    })


class TimeslotView(generics.ListAPIView):
    serializer_class = PlannerSerializer

    def get_queryset(self):
        """
        Takes the HTTP get request and returns a list of planner items
        :return: Serialized Planner Object
        """
        year = int(self.kwargs.get('year', 0))
        month = int(self.kwargs.get('month', 0))
        day = int(self.kwargs.get('day', 0))
        date = datetime(year=year, month=month, day=day).date()
        delivery = self.kwargs.get('delivery', None)
        if delivery:
            try:
                delivery = DeliveryTypes.objects.get(slug=delivery)
            except:
                delivery = None
        return Planner.objects.filter(id__in=getDaySlots(checkdate=date, deliverytype=delivery))


timeslot_return = TimeslotView.as_view()


class TimeSlotBook(mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   generics.GenericAPIView):
    serializer_class = PlannerSerializer
    queryset = Planner.objects.all()

    def get_object(self):
        """
        This takes the url information and returns the Planner option for this element
        :return: Planner object
        """
        year = int(self.kwargs.get('year', 0))
        month = int(self.kwargs.get('month', 0))
        day = int(self.kwargs.get('day', 0))
        slotid = int(self.kwargs.get('slot', 0))
        date = datetime(year=year, month=month, day=day).date()
        return Planner.objects.get(date=date, slot__id=slotid)

    def get(self, request, *args, **kwargs):
        """
        Takes the HTTP get request and returns the planner item
        :return: Serialized Planner Object
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        """
        This takes the HTTP post element and updates the elements
        :return: Serialized Planner Object
        """
        instance = self.get_object()
        if instance.taken is True:
            instance.taken = False
        elif instance.taken is False:
            instance.taken = True
        else:
            return Http404
        instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


timeslot_book = TimeSlotBook.as_view()

