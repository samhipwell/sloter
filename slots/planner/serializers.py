from rest_framework.serializers import *
from .models import *


class TimeSlotSerializer(ModelSerializer):
    class Meta:
        model = TimeSlot
        fields = '__all__'


class DeliveryTypeSerializer(ModelSerializer):
    class Meta:
        model = DeliveryTypes
        fields = '__all__'


class DeliveryDaysSerializer(ModelSerializer):
    deliverytype = DeliveryTypeSerializer(many=True)
    timeslot = TimeSlotSerializer(many=True)

    class Meta:
        model = DeliveryDays
        fields = '__all__'


class PlannerSerializer(ModelSerializer):
    slot = TimeSlotSerializer()

    class Meta:
        model = Planner
        fields = '__all__'
