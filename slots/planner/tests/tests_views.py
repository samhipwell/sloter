from django.test import TestCase
from slots.planner.views import *
from slots.planner.models import *
from datetime import datetime, timedelta
from django.conf import settings


class TestdayCount(TestCase):
    def test_basicdate(self):
        date = datetime(year=2017, month=12, day=9).date()
        self.assertEqual(daycount(date), 2)

    def test_basicdate_with_weekday(self):
        date = datetime(year=2017, month=12, day=9).date()
        weekday = 5 #Saturday
        self.assertEqual(daycount(date, weekday=weekday), 2)

    def test_basicdate_with_missmatch_day(self):
        date = datetime(year=2017, month=12, day=9).date()
        weekday = 2  # Saturday
        self.assertEqual(daycount(date, weekday=weekday), 0)

    def test_basicdate_with_weekday_greater_than_week(self):
        date = datetime(year=2017, month=12, day=9).date()
        weekday = 9
        self.assertEqual(daycount(date, weekday=weekday), 2)


class TestgetDaySlots(TestCase):
    def setUp(self):
        now = datetime.now()
        future = now + timedelta(hours=4)
        more_future = future +timedelta(hours=4)
        t1 = TimeSlot.objects.create(title='morning', start=now.time(), end=future.time())
        t2 = TimeSlot.objects.create(title='afternoon', start=future.time(), end=more_future.time())
        dt1 = DeliveryTypes.objects.create(name='normal')
        dt2 = DeliveryTypes.objects.create(name='special')
        dd1 = DeliveryDays.objects.create(day=0, allow_delivery=True)
        dd1.timeslot.add(t1)
        dd1.timeslot.add(t2)
        dd1.deliverytype.add(dt1)
        dd2 = DeliveryDays.objects.create(day=1, allow_delivery=True)
        dd2.timeslot.add(t1)
        dd2.timeslot.add(t2)
        dd2.deliverytype.add(dt1)
        dd3 = DeliveryDays.objects.create(day=2, allow_delivery=True)
        dd3.timeslot.add(t1)
        dd3.timeslot.add(t2)
        dd3.deliverytype.add(dt1)
        dd4 = DeliveryDays.objects.create(day=3, allow_delivery=True)
        dd4.timeslot.add(t1)
        dd4.timeslot.add(t2)
        dd4.deliverytype.add(dt1)
        dd5 = DeliveryDays.objects.create(day=4, allow_delivery=True)
        dd5.timeslot.add(t1)
        dd5.timeslot.add(t2)
        dd5.deliverytype.add(dt1)
        dd6 = DeliveryDays.objects.create(day=5, allow_delivery=True)
        dd6.timeslot.add(t1)
        dd6.timeslot.add(t2)
        dd6.deliverytype.add(dt1)
        dd7 = DeliveryDays.objects.create(day=6, allow_delivery=True)
        dd7.timeslot.add(t1)
        dd7.timeslot.add(t2)
        dd7.deliverytype.add(dt1)
        dd8 = DeliveryDays.objects.create(day=6, allow_delivery=False, nth_in_month=1)
        dd8.timeslot.add(t1)
        dd8.deliverytype.add(dt1)

    def test_checkdate_basic_not_input(self):
        self.assertIsInstance(getDaySlots(), list)

    def test_checkdate_basic_date(self):
        now = datetime.now().date()
        self.assertIsInstance(getDaySlots(checkdate=now), list)

    def test_checkdate_basic_date_elements(self):
        # checks to see if the right amount of elements returend
        now = datetime(year=2017, month=12, day=11).date()
        return_element = settings.DAY_GROUPING * TimeSlot.objects.count()
        self.assertEqual(len(getDaySlots(checkdate=now)), return_element)

    def test_checkdate_basic_date_elements_with_deliverytype(self):
        # checks to see if the right amount of elements returend
        now = datetime(year=2017, month=12, day=11).date()
        dt = DeliveryTypes.objects.get(id=1)
        return_element = settings.DAY_GROUPING * TimeSlot.objects.count()
        self.assertEqual(len(getDaySlots(checkdate=now, deliverytype=dt)), return_element)

    def test_checkdate_basic_date_elements_with_deliverytype_where_non_set_with_max_search(self):
        # checks to see if the right amount of elements returend
        now = datetime(year=2017, month=12, day=11).date()
        dt = DeliveryTypes.objects.get(id=2)
        return_element = settings.DAY_GROUPING * TimeSlot.objects.count()
        self.assertNotEquals(len(getDaySlots(checkdate=now, deliverytype=dt)), return_element)
