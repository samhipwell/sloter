from django.test import TestCase
from slots.planner.views import *
from slots.planner.models import *
from datetime import datetime, timedelta
from django.conf import settings


class TestgetTimeSlots(TestCase):
    def setUp(self):
        now = datetime.now()
        future = now + timedelta(hours=4)
        more_future = future +timedelta(hours=4)
        t1 = TimeSlot.objects.create(title='morning', start=now.time(), end=future.time())
        t2 = TimeSlot.objects.create(title='afternoon', start=future.time(), end=more_future.time())
        dt1 = DeliveryTypes.objects.create(name='normal')
        dt2 = DeliveryTypes.objects.create(name='special')
        dd1 = DeliveryDays.objects.create(day=0, allow_delivery=True)
        dd1.timeslot.add(t1)
        dd1.timeslot.add(t2)
        dd1.deliverytype.add(dt1)
        dd2 = DeliveryDays.objects.create(day=1, allow_delivery=True)
        dd2.timeslot.add(t1)
        dd2.timeslot.add(t2)
        dd2.deliverytype.add(dt1)
        dd3 = DeliveryDays.objects.create(day=2, allow_delivery=True)
        dd3.timeslot.add(t1)
        dd3.timeslot.add(t2)
        dd3.deliverytype.add(dt1)
        dd4 = DeliveryDays.objects.create(day=3, allow_delivery=True)
        dd4.timeslot.add(t1)
        dd4.timeslot.add(t2)
        dd4.deliverytype.add(dt1)
        dd5 = DeliveryDays.objects.create(day=4, allow_delivery=True)
        dd5.timeslot.add(t1)
        dd5.timeslot.add(t2)
        dd5.deliverytype.add(dt1)
        dd6 = DeliveryDays.objects.create(day=5, allow_delivery=True)
        dd6.timeslot.add(t1)
        dd6.timeslot.add(t2)
        dd6.deliverytype.add(dt1)
        dd7 = DeliveryDays.objects.create(day=6, allow_delivery=True)
        dd7.timeslot.add(t1)
        dd7.timeslot.add(t2)
        dd7.deliverytype.add(dt1)
        dd8 = DeliveryDays.objects.create(day=6, allow_delivery=False, nth_in_month=1)
        dd8.timeslot.add(t1)
        dd8.deliverytype.add(dt1)

    def test_weekday_brings_back_element(self):
        weekday = 2
        self.assertEqual(len(DeliveryDays.objects.getTimeSlots(weekday)), 2)

    def test_weekday_reduce_element(self):
        weekday = 6
        self.assertEqual(len(DeliveryDays.objects.getTimeSlots(weekday, 1)), 1)

    def test_weekday_nth_makes_differenct(self):
        weekday = 6
        nth = DeliveryDays.objects.getTimeSlots(weekday, 1)
        no_nth = DeliveryDays.objects.getTimeSlots(weekday)
        self.assertNotEquals(nth, no_nth)