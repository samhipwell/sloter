from django.contrib import admin
from .models import TimeSlot, DeliveryTypes, DeliveryDays, Planner
from django.utils.translation import ugettext_lazy as _


class TimeSlatAdmin(admin.ModelAdmin):
    list_display = ('title', 'start','end',)
    list_filter = ('start','end')
    fieldsets = (
        (_('Main'), {
            'classes': ('wide',),
            'fields': (
                'title',
                'start',
                'end',
            )
        }),
    )


class DeliveryTypesAdmin(admin.ModelAdmin):
    list_display = ('name',)
    fieldsets = (
        (_('Main'), {
            'classes': ('wide',),
            'fields': (
                'name',
                'slug',
            )
        }),
    )
    readonly_fields = ('slug',)


class DeliveryDaysAdmin(admin.ModelAdmin):
    list_display = ('day', 'deliverytypenames', 'timeslotnames', 'allow_delivery', 'nth_in_month',)
    list_filter = ('day', 'allow_delivery', 'timeslot', 'deliverytype',)
    fieldsets = (
        (_('Date time'), {
            'classes': ('wide',),
            'fields': (
                'day',
                'timeslot',
                'nth_in_month',
            )
        }),
        (_('Delivery'), {
            'classes': ('wide',),
            'fields': (
                'allow_delivery',
                'deliverytype',
         )
        }),
    )
    filter_horizontal = ('timeslot','deliverytype',)

    def deliverytypenames(self, obj):
        return ', '.join(obj.deliverytype.values_list('name', flat=True))

    def timeslotnames(self, obj):
        return ', '.join(obj.timeslot.values_list('title', flat=True))


class PlannerAdmin(admin.ModelAdmin):
    list_display = ('date', 'slot', 'taken',)
    list_filter = ('slot', 'taken',)
    date_hierarchy = 'date'
    fieldsets = (
        (_('Planner'), {
            'classes': ('wide',),
            'fields': (
                'date',
                'slot',
                'taken',
            )
        }),
    )


admin.site.register(TimeSlot, TimeSlatAdmin)
admin.site.register(DeliveryTypes, DeliveryTypesAdmin)
admin.site.register(DeliveryDays, DeliveryDaysAdmin)
admin.site.register(Planner, PlannerAdmin)