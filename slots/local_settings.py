from .default_settings import *
import os

DEBUG = True
SECRET_KEY = 'e&7f84i_171a_x3!(x67)0@no%!gl-8ql^de7t)uqw8@mtp9fj'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(BASE_DIR, 'logs/email')